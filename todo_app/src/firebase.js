// const firebaseConfig = {
//   apiKey: "AIzaSyAlzd5n-hGIE_BbG-uiFIKO8XcEVh16QRk",
//   authDomain: "todo-app-react-72fd0.firebaseapp.com",
//   databaseURL: "https://todo-app-react-72fd0.firebaseio.com",
//   projectId: "todo-app-react-72fd0",
//   storageBucket: "todo-app-react-72fd0.appspot.com",
//   messagingSenderId: "260086135734",
//   appId: "1:260086135734:web:bc06345dfa557800f91f17",
// };

import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAlzd5n-hGIE_BbG-uiFIKO8XcEVh16QRk",
  authDomain: "todo-app-react-72fd0.firebaseapp.com",
  databaseURL: "https://todo-app-react-72fd0.firebaseio.com",
  projectId: "todo-app-react-72fd0",
  storageBucket: "todo-app-react-72fd0.appspot.com",
  messagingSenderId: "260086135734",
  appId: "1:260086135734:web:bc06345dfa557800f91f17",
});

const db = firebaseApp.firestore();

export default db;
