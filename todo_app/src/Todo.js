import React from "react";
import "./Todo.css";
import db from "./firebase";
import {
  Button,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";

function Todo(props) {
  return (
    <List class="myClass">
      <ListItemAvatar></ListItemAvatar>
      <ListItem>
        <ListItemText
          primary={props.text.todo}
          secondary={"Dummy deadline 😄 "}
        />
      </ListItem>
      <Button
        onClick={(event) => {
          db.collection("todos").doc(props.text.id).delete();
        }}
      >
        <span>😏</span> Delete Me
      </Button>
    </List>
  );
}

export default Todo;
